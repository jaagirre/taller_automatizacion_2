# 00_ansible: Introduccion al aprovisionamiento de servidores

En esta carpeta se encuentra 5 ejercicios introductorios de Ansible.

1. Ejercicio1: Instalación de 2 Servidores web
2. Ejercicio2: Instalación de Wordpress
3. Ejercicio3: Instalación de Apache en servidores AWS EC2
4. Ejercicio4: Actualización de Servidores AWS EC2
5. Ejercicio5: Creación de una arquitectura HA en AWS

Para los ejercicios 1 y 2 se utilizara y aprovisionara el cluster de vagrant. Ene estos ejericios se utilizará la configuración _Cluster_ de vagrant del repositorio. El servidor Manager se utiliza para ejecutar Ansible y aprovisionar las tres maquinas que configurarán el cluster

Vagrantfile

```
Manager: 192.168.100.9
Manager2: 192.168.100.10
worker1: 192.168.100.11
worker2: 192.168.100.12
```

En los ejercicios 3,4 y 5 se necesita una cuenta de AWS.

## Prerequisito Cluster de servidores GNU/Linux

Para simular una red local con una serie de servidores se propone utilizar la configuracion vagrant _vagrant-cluster_ del repositorio _https://jaagirre@bitbucket.org/master_macc_devops/plataformas_e_infraestructuras.git_.

Una vez descargado el repositorio podeis lanzar el cluster. A continuacion se muestran los comandos basicos de gestion del cluster.

```bash
cd plataformas_e_infraestructuras\resources\basic
vagrant up
vagrant ssh manager
ubuntu@vagrant$ docker --version
ubuntu@vagrant$ docker system info
ubuntu@vagrant$ exit
vagrant halt
```

Para poder realizar la práctica se requiere disponer del cluster de vagrant y tener accesible en la maquina **manager** las claves SSH de las maquinas **Workers** del cluster, las cuales se ubican en **.vagrant\machines\worker1\virtualbox**. Lo mas facil es copiar dichas claves SSH a la carpeta donde se ubica el _vagrantfile_ de forma que en la maquina virtual manager apareceran en la carpoeta **/vagrant**.

Ademas de las claves SSH la maquina virtual **manager** debe de disponer de los scripts de este repositorio. Para ello copiar este repo a la carpeta donde se ubica el _vagrantfile_, de forma que en el servidor **manager** podemos acceder a los scripts utilizando **/vagrant/taller_2**.

Para comporbar que el entorno esta perparado para la realizacion de la práctica comporbar que se dispone de python y que la conexion SSH es posible.

```bash
# Copiar las calves privadas al manager al directorio de trabajo
# Las claves de las maquinas creadas se ubican en .vagrant\machines\worker1\virtualbox
vagrant up
vagrant ssh manager
chmod 400  private_key
ssh –i worker1_private_key vagrant@192.168.100.11
```

Para instalar **ansible** utilizar _apt install ansible_.

## Ejercicio1: Instalación de 2 Servidores web

En este ejercicio aprovisionaremos dos servidores del cluster con Apache y crearemos una pagina de inicio. Para se creare/utilizara un playbook de ansible **apache_playbook.yml**. Los servidores a aproviosnar se encuentran en el fichero **host** donde tambien se especifica la configuracion de la conexion SSH. En el fichero **ansible.cfg** se especifica el usuario SSH a utilizar.

En el fichero **apache_playbook.yml** teneis la descripcion del ansible que instala un apache  y la pagina de inicio.

Para aprovisionar los dos servidores ejecutar el siguiente comando desde el servidor _manager_.

```bash
cd /vagrant/taller_2/00_ansible
cd ejercicio1
cp /vagrant/worker1_private_key ./
cp /vagrant/worker2_private_key ./
#probar conectivudad
ping 192.168.100.11
# debido a que no podemos modificar los permisos de carpetas compoartidas con windows
# se debe de copiar el proyecto al  home de vagrant
mkdir /home/vagrant/taller2
cp -rf /vagrant/taller_2/* /home/vagrant/taller2
cd $pwd
cd taller2/00_ansible/ejercicio1
chmod 400 *private_key
# probar onectividad ssh , necesaria para ansible
ssh -i private_key_worker1 vagrant@192.168.100.11
# finalmente desde el manager ejecutar el playbook
 ansible-playbook -i hosts apache_playbook.yml
```

La salida debe de ser 

```bash
TASK [Sortu ongi etorri index.html] *********************************************************************************
ok: [worker1]
ok: [192.168.100.12]

TASK [restart apache2] **********************************************************************************************
changed: [192.168.100.12]
changed: [worker1]

PLAY RECAP **********************************************************************************************************
192.168.100.12             : ok=5    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
worker1                    : ok=5    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

Y para probar el servidor  pedir mediante el navegador o curl la pagina index.html a los servidores
```bash
curl 192.168.100.12
kaixo Howdy from  worker2
curl 192.168.100.11
kaixo Howdy from  worker1
```

## Ejercicio2: Instalación de Wordpress

En este ejericio se instala un wordpress los dos PCs workers. Esta instalacion es mas compleja y requiere instalar wordpress, php y mysql. Por ello el playbook de instalación se dividara en varios Roles. Dentro de la carpeta roles teneis la especificación de cada uno de los roles.

```bash
- mysql/ 
- php
- server
- wordpress
```

Para ejecutar este playbook consituido por 4 roles ejecutar los siguiente socmandos.

```bash
cd /home/vagrant/taller2/00_ansible/ejercicio2
cp /vagrant/worker1_private_key ./
cp /vagrant/worker2_private_key ./
chmod 400 worker*
ansible-playbook -i hosts playbook.yml -u vagrant
```

Esta es la salida (En alguna de las pruebas que he realziado en diferentes PC he tenido que ejecutar 2 veces el playbook)
```bash
TASK [wordpress : WordPress config file] ****************************************************************************
changed: [worker1] => (item={u'regexp': u"define\\( 'DB_NAME', '(.)+' \\);", u'line': u"define('DB_NAME', 'wordpress');"})
changed: [192.168.100.12] => (item={u'regexp': u"define\\( 'DB_NAME', '(.)+' \\);", u'line': u"define('DB_NAME', 'wordpress');"})
changed: [worker1] => (item={u'regexp': u"define\\( 'DB_USER', '(.)+' \\);", u'line': u"define('DB_USER', 'wordpress');"})
changed: [192.168.100.12] => (item={u'regexp': u"define\\( 'DB_USER', '(.)+' \\);", u'line': u"define('DB_USER', 'wordpress');"})
changed: [worker1] => (item={u'regexp': u"define\\( 'DB_PASSWORD', '(.)+' \\);", u'line': u"define('DB_PASSWORD', 'password');"})
changed: [192.168.100.12] => (item={u'regexp': u"define\\( 'DB_PASSWORD', '(.)+' \\);", u'line': u"define('DB_PASSWORD', 'password');"})

RUNNING HANDLER [wordpress : restart apache2] ***********************************************************************
changed: [192.168.100.12]
changed: [worker1]

PLAY RECAP **********************************************************************************************************
192.168.100.12             : ok=13   changed=11   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
worker1                    : ok=13   changed=11   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

Finalmente para probar los servidores navegar a cada uno de los servidores y pedir las paginas de wordpress.

url: http://192.168.100.12/wp-admin/install.php
url: http://192.168.100.11/wp-admin/install.php

## Ejercicio3:  Instalación de Apache en servidores AWS EC2

En este ejercicio se realiza la instalación de Apache en servidores AWS EC2.  En este playbook compuesto por roles se realiza lo siguiente:

- Se crean dos maquinas EC2 en una subred publica en la VPC por defecto
- Como se utilizan IP dinamicas se creara un inventario de hosts dinamicos, para obtener los datos se utilizan facts de ansible
- Despues se aprovisionan los servidores
- Y se crean tareas de rollback que permiten destruir la infraestructura, ya que ansible no ofrece tarea de rollback.

Para poder ejecutar el playbook tenemos que configurar la maquina manager con las credenciales de AWS de vuestra cuenta. Para ello deberies de modificar el fichero **.aws/credenetials** con vuestros datos.

En la presentacion teneis indicado podemos podemos utilziar **ansible-vault** para guardar de forma segura datos de password y claves a utilizar en ANSIBLE, pero con Tokens temporales no se puede llevar a cabo , por lo tanto realizar este ejercicio sin vaults. Dependiendo de la version es preferible utilizar varaibles de entorno para establecer los **ACCESS_KEY y TOKENS**. Para ello , una vez puestos los CCESS_KEY y TOKENS en .aws/credenetial ejecutar los siguientes comandos en el bash para exportar las variables necesaria por ANSIBLE para AWS.

```bash
export AWS_ACCESS_KEY_ID=$(sed -n -e 's/aws_access_key_id=//p' < ~/.aws/credentials)
export AWS_SECRET_ACCESS_KEY=$(sed -n -e 's/aws_secret_access_key=//p' < ~/.aws/credentials)
export AWS_SESSION_TOKEN=$(sed -n -e 's/aws_session_token=//p' < ~/.aws/credentials)

```
Para ejecutar  los pplaybooks de AWS es necesario tenerinstalado la libreria **boto3**

```bash
pip install boto3
pip install awscli
```

Ademas de las credenciales necesitamos especificar la clave de acceso a las maquinas para que ansible pueda realziar ssh a las maquinas EC2. Para ello deberis de ofrecer la clave PEM de accesso SSH. En el fichero **ansible.cfg** espeicicais la ubicacion del PEM, por si lo modificais. Por ejemplo en mi caso he debido de copiar el siguiente fichero:
```bash
cp  /vagrant/educate_1.pem /home/vagrant/taller2/00_ansible/ejercicio3/educate_1.pem
chmod 400 /home/vagrant/taller2/00_ansible/ejercicio3/educate_1.pem
```
En la presenetacion de Mudle de la asignatura correspondiente a ANSIBLE teneis una descripcion de los roles y playbooks de este ejercicio.

Para ejecutar el playbook simplemente ejecutar el siguiente comando y esperar a que se cree la infraestructura.

```bash
ansible-playbook -i hosts aws_provision.yml
```
Tras ejecutar este comando ontendreis una salida similar a la siguiente. En mi caso ,en una de las ejecuciones solo ha realizado correctamente una de las instalaciones.

```bash
TASK [install python] ***********************************************************************************************
fatal: [3.95.181.212]: FAILED! => {"changed": true, "msg": "non-zero return code", "rc": 100, "stderr": "Warning: Permanently added '3.95.181.212' (ECDSA) to the list of known hosts.\r\nShared connection to 3.95.181.212 closed.\r\n", "stderr_lines": ["Warning: Permanently added '3.95.181.212' (ECDSA) to the list of known hosts.", "Shared connection to 3.95.181.212 closed."], "stdout": "E: Could not get lock /var/lib/dpkg/lock-frontend - open (11: Resource temporarily unavailable)\r\nE: Unable to acquire the dpkg frontend lock (/var/lib/dpkg/lock-frontend), is another process using it?\r\n", "stdout_lines": ["E: Could not get lock /var/lib/dpkg/lock-frontend - open (11: Resource temporarily unavailable)", "E: Unable to acquire the dpkg frontend lock (/var/lib/dpkg/lock-frontend), is another process using it?"]}
changed: [52.207.145.200]

TASK [Install Apache] ***********************************************************************************************
WARNING]: Updating cache and auto-installing missing dependency: python-apt

[DEPRECATION WARNING]: Distribution Ubuntu 16.04 on host 52.207.145.200 should use /usr/bin/python3, but is using
/usr/bin/python for backward compatibility with prior Ansible releases. A future Ansible release will default to
using the discovered platform python for this host. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information. This
feature will be removed in version 2.12. Deprecation warnings can be disabled by setting deprecation_warnings=False
in ansible.cfg.
changed: [52.207.145.200]

TASK [service] ******************************************************************************************************

ok: [52.207.145.200]

PLAY RECAP **********************************************************************************************************
3.95.181.212               : ok=0    changed=0    unreachable=0    failed=1    skipped=0    rescued=0    ignored=0
52.207.145.200             : ok=3    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
localhost                  : ok=6    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```
Ahora utilizandola ip publica comprobar que el servidor apache responde correctamente.En uno de mis casos, 

http://52.207.145.200/

Disponeis del playbook ec2_down.yml para finalizar todas las instancias EC2.

```bash
ansible-playbook -i hosts ec2_down.yml

PLAY RECAP **********************************************************************************************************
localhost                  : ok=4    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```


## Ejercicio4: Actualización de Servidores AWS EC2
El objetivo de está práctica es actualizar algunos servidores EC2 una vez creados y sin conocer sus IP. Se utilizar el Inventory Dinámico ,  en este caso, mediante
los scripts ec2.py  y ec2.ini.

El primer paso de la práctica es descargar los scripts ec2.py y ec2.ini:

```bash
wget https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inven tory/ec2.py
wget https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inven tory/ec2.ini
```

Ahora le indicaremos a ANISBLE que utilize estos scripts para realizar el inventory de HOSTS. Para ello copiamos el script **ec2.py** a ***/etc/ansible/** y exportamos las siguientes variables del sistema:
```bash
export ANSIBLE_HOSTS=/etc/ansible/ec2.py
export EC2_INI_PATH=/etc/ansible/ec2.ini

```

Para verificar que funciona ejecutar el script ec2.py. Anets de ejecutar verificar el fichero de configuracion **ec2.ini**:
- Regiones validas
- rds=false
- Elasticcache=false


```bash
ec2.py --list
```


Despues agregagmos la clave SSH que utilizaremos en las instancias EC2.
```
ssh-agent bash 
ssh-add ~/.ssh/educate_1.pem 
```
Ahora comprobaremos la conectividad SSH ejecutando un ping en los ervidores

```bash
ansible -m ping -i /etc/ansible/ec2.py tag_*  -u ubuntu
```

Y ahora vamos a ejejcutar una actualziacion en aquellos  servidores que tengan el TAG  Type=webserver

```bash
ansible -m apt -a "name=* state=latest" tag_* -i /etc/ansible/ec2.py -u ub untu -b --become-method=sudo --become-user=root

```

Finalmente ejecutaremos un playbook utilizando el hosts inventory ec2.py

```
ansible-playbook playbook.yml -i /etc/ansible/ec2.py

```




## Ejercicio5: Creación de una arquitectura HA en AWS

En este ejercicio se plantea crear una arquitectura HA para wordpress con Ansible


- Crear un VPC
- Crear subredes
- 1 pública
- 2 privadas
- 1 IGW
- Tablas de rutado
- Grupos de seguridad
- Crear un bastion SSH
- Crear un  ELB
- Crear dos servidores WEB

Para la realizacion de esta prñactica seguir los pasos indcados en la presentacion sobre ANISBLE que teneis en el mudle de la asignatura.

En esta carpeta teneis diferentes soluciones para este ejercicio , por ejemplo  todo con IPs publicas o un bastion con publica y los demas con privadas, etc.

