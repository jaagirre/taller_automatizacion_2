#!/bin/bash

echo 'Estableciendo credenciales AWS...'

export AWS_ACCESS_KEY_ID=$(sed -n -e 's/aws_access_key_id=//p' < ~/.aws/credentials)
export AWS_SECRET_ACCESS_KEY=$(sed -n -e 's/aws_secret_access_key=//p' < ~/.aws/credentials)
export AWS_SESSION_TOKEN=$(sed -n -e 's/aws_session_token=//p' < ~/.aws/credentials)

#export AWS_ACCESS_KEY_ID=$(aws --profile=REPLACE-ME configure get aws_access_key_id)
#export AWS_SECRET_ACCESS_KEY=$(aws --profile=REPLACE-ME configure get aws_secret_access_key)
#export AWS_SESSION_TOKEN=$(aws --profile=REPLACE-ME configure get aws_session_token)