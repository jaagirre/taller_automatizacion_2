# Taller Automatizacion 2

En este repositorio se encuentran los recursos necesarios para el segundo taller sobre automatización de infraestructuras. El objetivo de este taller es realizar una introducción a dos herramientas para el **aprovisionamiento de servidores** y el **aprovisionamiento de infraestructura**. Tambien disponeis de dos presentaciones en el Mudle de la asignatura que os guian en la realización de estas prácticas.

- Aprovisionamiento de servidores **Ansible**
- Aprovisionamiento de infraestructura **Terraform**

En la carpeta **00_ansible** estan los scripts necesarios para la realizaciçon de una serie de ejercicios básicos de aprovisionamient de servidores mediante Ansible, se aproviosarám servidores locales como AWS. Mientras que en la carpeta **01_terraform** se encuentran un proyecto para el aprovisionamiento de infraestructura en AWS.

Tanto la carpeta **00_ansible** como la carpeta **01_terraform** presnetan un README.md con la información necesaria parala realización de todos los ejercicios.

## 00_ansible

En esta carpeta se encuentra 5 ejercicios introductorios de Ansible.

1. Ejercicio1: Instalación de 2 Servidores web
2. Ejercicio2: Instalación de Wordpress
3. Ejercicio3: Actualización de Servidores AWS EC2
4. Ejercicio4: Instalación de Apache en servidores AWS EC2
5. Ejercicio5: Creación de una arquitectura HA en AWS

## 01_terraform

En esta carpeta se encuentra 1 ejercicio introductorio sobre el aprovisionamiento de infraestructura AWS mediante Terraform.

1. Ejercicio1: VPC, EC2 y RDS
