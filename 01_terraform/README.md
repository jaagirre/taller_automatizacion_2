# 01_terraform: Introducción al aprovisionamiento de infraestructra mediante Terraform

En esta carpeta se encuentra 1 ejercicio introductorio sobre el aprovisionamiento de infraestructura AWS mediante Terraform.

1. Ejercicio1: VPC, EC2 y RDS

Para la realziación de estas prácticas se necesita una cuenta AWS.

## Ejercicio1: VPC, EC2 y RDS


Para introducirnos a Terraform realizaremos un ejercicio donde crearemos:
- Infraestructura AWS VPC
- Infraestructura AWS EC2 y RDS

Para realizar esta práctica utilizar la informacion que teneis en la presentacion sobre Terraform que teneis en el Mudle de la asignatura.
