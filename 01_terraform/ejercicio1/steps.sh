cat <<-'AWSPROFILE' > ~/terraform/tf-ejercicio1/infra/aws.tf
variable "region" {}
provider "aws" {
  region     = "${var.region}"
}
AWSPROFILE
cp ~/terraform/tf-ejercicio1/infra/aws.tf ~/terraform/tf-ejercicio1/webapp/aws.tf

variable "region" {}
provider "aws" {
  region     = "${var.region}"
}

#instalar AWS
pushd ~/terraform/tf-ejercicio1/infra/ && terraform init && popd
pushd ~/terraform/tf-ejercicio1/webapp/ && terraform init && popd

#Desplegando el VPC

# setup variables
export AWS_DEFAULT_PROFILE="learning"
export AWS_PROFILE=$AWS_DEFAULT_PROFILE
export TF_VAR_profile=$AWS_DEFAULT_PROFILE
export TF_VAR_region=$(
  awk -F'= ' '/region/{print $2}' <(
    grep -A1 "\[.*$AWS_PROFILE\]" ~/.aws/config)
)
# initialize modules and see changes
cd ~/tf-projects/infra
terraform init
terraform plan
# create infrastructure
terraform apply
# cleanup infrastructure
terraform destroy

# Creacion de estructura de y ficheros webapp
cd webapp
touch {.,app,db}/{main.tf,variables.tf} 
touch app/user_data.sh

#ejecutar script fina
# show changes required (using db variables file)
terraform plan -var-file="db.tfvars"
# apply changes required (using db variables file)
terraform apply -var-file="db.tfvars"


terraform show | grep -o 'public_ip = .*$'

terraform show | grep -o 'endpoint = .*$'
my-db.cknof0oc3nnn.us-east-2.rds.amazonaws.com:3306
